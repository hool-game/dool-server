## Setup Local Development Environment

This server depends on the following services:

- Redis with JSON plugin
- XMPP server

To simplify the setup, we have provided a docker-compose file that will start the required services.
But if you wish you can install redis manually and configure it. For XMPP server, you can connect to the beta server.

### With Docker

Run this command to start/build the docker container

```bash
cd docker/
docker compose up
```

This is how your `.env` file should look like:

```env
DOOL_XMPP_SERVICE= xmpp://127.0.0.1:5347
DOOL_COMPONENT_DOMAIN= "local.host.com"
DOOL_COMPONENT_PASSWORD="localhost@123!"
```

You can change or add the domain and password in the docker image at /etc/metronome/metronome.cfg.lua.
Or, you can modify the metronome-localhost.cfg.lua file in the /docker folder and rebuild the image.

````bash

### Without Docker

#### Redis
Install Redis on Linux, macOS, and Windows
https://redis.io/docs/getting-started/installation/

After installing redis, we have to add a json plugin for Redis
download the plugin from this link
https://upload.disroot.org/r/HNNc1yve#IgwZdPpCocv6i4Y3tqgswpyqXZexMBlq+NuFZ+Li9Cw=

download the librejson.so from the link and move the file to /etc/redis (filePath) for linux

open the redis.conf file (/etc/redis/redis.conf)
add the module under ####MODULE####
loadmodule /etc/redis/librejson.so

Your .env file should look like this:

```env
DOOL_XMPP_SERVICE= xmpp://beta.hool.org
DOOL_COMPONENT_DOMAIN= "<name>.hool.org"
DOOL_COMPONENT_PASSWORD="<component_$ecret>"
````

Where you can get the component domain and password from an admin.
