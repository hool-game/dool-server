import { jid } from "@xmpp/component"
import { EventEmitter } from "events"

import cards from "@firtoska/cards"
import Dool from "@dool/logic"

// Suits map for checking is this a valid one!
const SUITS = new Map([
  ['C', 'clubs'],
  ['D', 'diamonds'],
  ['H', 'hearts'],
  ['S', 'spades'],
])

// Undo Class, for adding it to trick
class Undo {
  constructor(player, rejected, accepted, mode) {
    this.player = player;
    this.rejected = rejected;
    this.accepted = accepted;
    this.mode = mode;
  }
}

export class DoolEngine extends EventEmitter {
  /**
   * Constructor for DoolEngine
   *
   * Quickly set up and listen for Dool events.
   * Currently supported options (inside the
   * `options` object) are:
   *
   * - redisConnection - the database connection
   * - transmitter - a DoolTransmitter object
   *
   * More options may be added later.
   */

  constructor(options = {}, ...args) {
    // call the parent constructor first
    super(...args);

    if (!!options.db) {
      this.setDB(options.db)
    }

    if (!!options.transmitter) {
      this.listen(options.transmitter)
    }
  }

  /**
   * Connect to the Redis database
   */
  setDB(redisConnection) {
    this.db = redisConnection
  }

  /**
   * Process events
   */

  processEvents(events) {
    events.forEach((e) => {
      this.emit(e.event, e)
    });
  }

  // room list functions (for jabber:iq:search)
  async getRooms() {
    return []; // TODO: implement
  }

  async getRoom(roomID, create = false) {
    // First, check if the room already exists
    let room = await this.db.json.get(`room:${roomID}`)

    /*
     * Create it if we have to
     *
     * Note that this only creates a placeholder
     * to be manipulated; it doesn't actually save
     * it to the database or anything like that
     */
    if (create && !room) {
      return {
        occupants: [],
        messages: [],
        game: {}, // empty game
      };
    }

    // Process the data a bit
    if (room) {
      // Unflatten the deck
      if (room.deck) {
        let originalDeck = room.deck
        room.deck = new cards.Deck()
        room.deck.cards = originalDeck
      }

      // Unflatten tricks
      if (room.tricks) {
        let originalTricks = room.tricks
        room.tricks = new cards.Tricks()

        // this is needed for setting trump
        if (room.meta?.trumpSuit) {
          room.tricks.trumpSuit = room.meta.trumpSuit
        }

        room.tricks.loadArray(originalTricks)

        // Flip appropriate tricks
        if (room.meta?.flippedTricks) {
          room.meta.flippedTricks.forEach((flipped, i) => {
            if (flipped && room.tricks[i]) {
              room.tricks[i].flipped = true
            }
          });

          // Don't forget to flip the tricks object itself!
          if (
            room.tricks.length &&
            room.tricks[room.tricks.length - 1].flipped
          ) {
            room.tricks.flipped = true
          }
        }

        // TODO: add claim to the appropriate tricks
        // Loop through the claimedTricks from meta and 
        // create a Claim instance of the objects (if it is there)
        // and add it to the Trick
        if (room.meta?.claimedTricks) {
          room.meta.claimedTricks.forEach((trick, i) => {
            if (room.tricks[i]) {
              trick.forEach((claim, j) => {
                if (
                  // do the check here for empty object!
                  Object.keys(claim).length !== 0
                ) {
                  const claimObj = new cards.Trick().addClaim(claim.proposal, claim.acceptedBy, claim.rejectedBy, claim.participants);
                  room.tricks[i][j] = claimObj
                }
              })
            }
          })
        }

        if (room.meta?.undoTricks) {
          room.meta.undoTricks.forEach((trick, i) => {
            if (room.tricks[i]) {
              trick.forEach((undo, j) => {
                if (
                  // do the check here for empty object!
                  Object.keys(undo).length !== 0
                ) {
                  const undoObj = new Undo(undo.player, undo.rejected, undo.accepted, undo.mode);
                  room.tricks[i][j] = undoObj
                }
              })
            }
          })
        }
      }
    }

    // console.log("getting room for ", room)
    // Return it
    return room
  }

  async setRoom(roomID, data) {
    // Don't munge the data
    let d = { ...data }

    // Extra data that should not be used outside the
    // getRoom and setRoom functions
    d.meta = {};

    // Flatten the deck
    if (d.deck && d.deck.cards) {
      d.deck = d.deck.cards
    }

    // Flatten tricks
    if (d.tricks && d.tricks.getTricksArray) {
      // First, note down which tricks are flipped
      d.meta.flippedTricks = d.tricks.map((trick) => trick.flipped)

      // store down claim also
      d.meta.claimedTricks = d.tricks.map(trick => {
        return trick.map(element => {
          if (element.proposal !== undefined) {
            return {
              proposal: element.proposal,
              acceptedBy: element.acceptedBy,
              rejectedBy: element.rejectedBy,
              participants: element.participants
            };
          } else {
            //done to preserve historical sequence
            return {};
          }
        });
      });

      d.meta.undoTricks = d.tricks.map(trick => {
        return trick.map(element => {
          if (element.mode !== undefined) {
            // console.log("undo element:  ", element)
            return {
              mode: element.mode,
              accepted: element.accepted,
              rejected: element.rejected,
              player: element.player,
            };
          } else {
            //done to preserve historical sequence
            return {};
          }
        });
      });

      // We setting the trumpSuit in handleStart(),
      // but we need to set it here also to save the trump
      d.meta.trumpSuit = d.tricks.trumpSuit

      // Then, flatten them
      d.tricks = d.tricks.getTricksArray()
    }

    // Save it
    return await this.db.json.set(`room:${roomID}`, "$", d);
  }

  async delRoom(roomID) {
    await this.db.del(`room:${roomID}`)
  }

  // Helper function to send state updates
  renderState(roomID, room, userIndex, render = []) {
    // Check the user index
    if (!room.occupants[userIndex]) {
      throw `Invalid userIndex: ${userIndex}`
    }

    // Decide what to render
    let toRender = new Map(
      ["occupants", "ready", "deck", "hands", "bids", "score", "tricks", "subHand", "scoreHands"].map(
        (p) => [p, false]
      )
    );

    // Parse the "render" argument
    if (render == "all") {
      // 'all' is shorthand for rendering everything
      toRender.forEach((_, r) => toRender.set(r, true));
    } else if (typeof render == "string") {
      // If it's a string, we just render that one thing
      toRender.set(render, true)
    } else {
      // Otherwise, mark each thing separately
      render.forEach((r) => {
        // 'game' is shorthand for hands, bids, and tricks
        if (r == "game") {
          ["hands", "bids", "score", "tricks", "deck"].forEach((s) =>
            toRender.set(s, true)
          );
        } else if (r == "claim") {
          ["hands", "score", "runningScore"].forEach((s) =>
            toRender.set(s, true)
          );
        } else {
          toRender.set(r, true)
        }
      });
    }

    // Now, we have a handy map of what to render and
    // what to ignore.

    let events = []

    // First, get some basic info
    let user = room.occupants[userIndex].jid
    let nickname = room.occupants[userIndex].nickname

    let side = -1;
    if (typeof room.occupants[userIndex].player == "number") {
      side = room.occupants[userIndex].player
    }

    // Start with sending the occupant list
    if (toRender.get("occupants")) {
      room.occupants.forEach((o) => {
        events.push({
          event: "roomJoined",
          to: user,
          user: o.jid,
          room: roomID,
          role: o.role || "rover",
          nickname: o.nickname,
        });
      });
    }

    // Next, tell if they're ready to play
    if (toRender.get("ready")) {
      room.occupants.forEach((o) => {
        if (!!o.ready) {
          events.push({
            event: "start",
            to: user,
            nickname: o.nickname,
            room: roomID,
          })
        }
      })
    }

    // Now it's time to start rendering the state!
    if (
      ["deck", "hands", "bids", "score", "tricks", "subHand", "scoreHands"].some((r) => toRender.get(r))
    ) {
      // Set up the event itself
      let state = {
        event: "state",
        user: user,
        room: roomID,
      };

      // Begin with the deck
      if (
        toRender.get("deck") &&
        !!room.deck?.cards // The deck should exist!
      ) {
        state.deck = room.deck
      }

      // For exchanging cards: 
      if (
        toRender.get("subHand") &&
        !!room.players // The players should exist!
      ) {
        state.players = []

        room.players.forEach((p, i) => {
          let player = {}
          player.nickname = p.nickname
          player.inactive = p.inactive
          player.index = i
          player.team = p.team
          player.swap = p.swap

          if (toRender.get("subHand")) {

            // Only show hands if it's an own/team hand, the
            // user is a non-player, or the swap is true
            if (
              (p.swap && side == i % 2) ||
              side < 0
            ) {
              player.hand = p.hand
            }
          }

          // Don't forget to add it all in
          state.players.push(player)
        });
      }

      // Move on to the per-player data: hands and bids
      if (
        ["hands", "bids"].some((r) => toRender.get(r)) &&
        !!room.players // The players should exist!
      ) {
        state.players = []

        room.players.forEach((p, i) => {
          let player = {}
          player.nickname = p.nickname
          player.inactive = p.inactive
          player.index = i
          player.team = p.team
          player.swap = p.swap

          // in this we have to add / send the partner's hand 
          // for showing it in the front-end.
          // add the logic for that.
          // Include hands
          if (toRender.get("hands")) {
            // Only show hands if it's an own hand, the
            // user is a non-player, or the hand is open
            if (
              side == i % 2 ||
              side < 0 || p.open
            ) {
              player.hand = p.hand
            }
          }

          // Don't forget to add it all in
          state.players.push(player)
        });

        // send the dealNumber to the state
        if (room.dealNumber !== undefined) {
          state.dealNumber = room.dealNumber
        }
      }

      // Move on to the per-player data: hands and bids
      if (
        toRender.get("scoreHands") &&
        !!room.players // The players should exist!
      ) {
        state.scorePlayers = []

        room.dummyPlayers.forEach((p, i) => {
          let player = {}
          player.nickname = p.nickname
          player.inactive = p.inactive
          player.index = i
          player.team = p.team

          // in this we have to add / send the partner's hand 
          // for showing it in the front-end.
          // add the logic for that.
          // Include hands
          if (toRender.get("scoreHands")) {
            // Only show hands if it's an own hand, the
            // user is a non-player, or the hand is open
            if (
              side == i % 2 ||
              side < 0 || p.open
            ) {
              player.hand = p.hand
            }
          }

          // Don't forget to add it all in
          state.scorePlayers.push(player)
        });
      }

      // set the dealer to state:
      // Will help in finding who's the next dealer
      if (room.dealer != undefined) {
        state.dealer = room.dealer
      }

      // Now we have to send the bids for catchup or non-player
      if (toRender.get("bids") && !!room.bids) {
        state.bids = []

        room.bids.forEach((bid) => {
          state.bids.push({
            player: bid.player,
            type: bid.bidType,
            level: bid.level,
            suit: bid.suit
          })
        })

        // Set the contract to state
        // Will help in deciding winner
        if (room.contract) {
          state.contract = room.contract
        }

        // set the leader to state
        if (room.leader != undefined) {
          state.leader = room.leader
        }

        // Set the running score to state
        if (room.runningScore) {
          // assign the score array
          state.runningScore = room.runningScore
        }

        // set handLength
        if (room.handLength) {
          state.handLength = room.handLength
        }
      }

      if (
        ["hands", "runningScore", "score"].some((r) => toRender.get(r)) &&
        !!room.players
      ) {
        // Set the running score to state
        if (room.runningScore) {
          // assign the score array
          state.runningScore = room.runningScore
        }

        if (toRender.get("score") && !!room.score) {
          state.scores = room.score
        }

        // send the dealScores
        if (room.dealScores) {
          state.dealScores = room.dealScores
        }
      }

      // Now the players are rendered (in case you lost track).
      // All that's left is to do the tricks

      if (
        toRender.get("tricks") &&
        !!room.tricks // the tricks should exist!
      ) {
        // Nothing much to do here; it's already formatted
        // properly for us (at least I hope so!)
        state.tricks = room.tricks
      }

      if (toRender.get("score") && !!room.score) {
        state.scores = room.score
      }

      // That's it for the state update
      events.push(state)
    }

    // ...and we're done!
    return events
  }

  /**
   * Let the games begin!
   */
  async handleRoomJoin(request) {
    console.debug(
      `${request.user} wants to join ${request.room} as ${request.role}`
    );
    let events = []
    let room = await this.getRoom(request.room, true)

    // If the user is already there, add them anyway
    let userIndex = room.occupants.findIndex(
      // (o) => o.jid && o.jid == jid(request.user).bare().toString()
      (o) => o.nickname == request.nickname
    );

    if (userIndex !== -1) {
      // User is already in the room, check if the role has changed
      const previousRole = room.occupants[userIndex].role;
      
      // Role has changed, update the user's role
      if (!["player", "watcher"].includes(previousRole)) {
        room.occupants[userIndex].role = request.role;
        room.occupants[userIndex].jid = jid(request.user).bare().toString();

        // set the updated occupants role to db!
        this.setRoom(request.room, room)

        // Tell everyone about it
        room.occupants.forEach((o) => {
          // user -> everyone (except self)
          if (o.jid == jid(request.user).bare()) return

          events.push({
            event: "roomJoined",
            to: o.jid,
            user: request.user,
            room: request.room,
            role: request.role || "rover",
            nickname: request.nickname,
          });
        });
      }
    }

    if (userIndex >= 0) {
      room.occupants[userIndex].jid = jid(request.user).bare().toString()

      // set the updated occupants' jid to db!
      this.setRoom(request.room, room)

      // Send the game state, just in case (this will
      // include a "you joined" event as well as all
      // the "someone else joined" events for everyone
      // else.
      events = events.concat(
        this.renderState(request.room, room, userIndex, "all")
      );

      return events
    } else {
      // events = renderState("all")
      // return events
    }

    // Check if we're full
    if (room.occupants.length >= 6) {
      console.debug("Skipping: room is full")

      // TODO: allow them to join as non-players

      // Send "room full" message
      events.push({
        event: "roomFull",
        room: request.room,
        user: request.user,
      });

      // TODO: remove this line
      this.delRoom(request.room) // clearing for testing

      return events;
    }

    // Check if the game has already started

    if (room.started) {
      // TODO: if game has started means
      /**
       * Default role - `rover / none`
       * there are already two players playing the game.
       * So, if the new user joins he needs to be a
       * kibitzer, change his role `player` from `watcher`
       */
      
      room.occupants.push({
        jid: jid(request.user).bare().toString(),
        role: "watcher", // TODO: change default
        nickname: request.nickname,
      });

      console.debug("Not Banning: game has already started, so set them as `watcher`")

      // Note down the userIndex
      userIndex = room.occupants.length - 1
      // ^-- that was mildly unsafe but should be okay

      // Save the room
      this.setRoom(request.room, room)

      // Tell everyone about it
      room.occupants.forEach((o) => {
        // user -> everyone (except self)
        if (o.jid == jid(request.user).bare()) return

        events.push({
          event: "roomJoined",
          to: o.jid,
          user: request.user,
          room: request.room,
          role: "watcher",
          nickname: request.nickname,
        });
      });

      // Send the user a full update
      events = events.concat(
        this.renderState(request.room, room, userIndex, "all")
      );

      return events
    }

    // TODO: handle mid-game joins

    // All good; add us in
    room.occupants.push({
      jid: jid(request.user).bare().toString(),
      role: request.role || "rover", // TODO: change default
      nickname: request.nickname,
    });

    // Note down the userIndex
    userIndex = room.occupants.length - 1
    // ^-- that was mildly unsafe but should be okay

    // Save the room
    this.setRoom(request.room, room)

    // Tell everyone about it
    room.occupants.forEach((o) => {
      // user -> everyone (except self)
      if (o.jid == jid(request.user).bare()) return

      events.push({
        event: "roomJoined",
        to: o.jid,
        user: request.user,
        room: request.room,
        role: request.role || "rover",
        nickname: request.nickname,
      });
    });

    // Send the user a full update
    events = events.concat(
      this.renderState(request.room, room, userIndex, "all")
    );

    return events
  }

  async handleRoomExit(request) {
    console.debug(`${request.user} has left ${request.room}.`)

    let events = []

    // fetch the room
    let room = await this.getRoom(request.room)

    // if the room doesn't exist, there's nothing to do!
    if (!room) {
      console.debug(`${request.room} does not exist; ignoring.`)
      return events
    }

    // find the user
    let userIndex = room.occupants.findIndex(
      (o) => o.jid && o.jid == jid(request.user).bare().toString()
    );

    // if the user is not in the room, there's nothing to do!
    if (userIndex < 0) {
      console.debug(`${request.user} is not in ${request.room}; ignoring.`)
      return events
    }

    // remove the user
    let user = room.occupants.splice(userIndex, 1)[0]

    // find out how many users are left
    if (room.occupants.length == 0) {
      // no users? destroy the table!

      this.delRoom(request.room)
      return events
    } else {
      // tell everyone
      room.occupants.forEach((o) =>
        events.push({
          event: "roomExited",
          to: o.jid,
          user: user.jid,
          room: request.room,
          nickname: user.nickname,
        })
      )

      // if the disconnected user is a player means
      // we have to remove the user from player list and
      // reveal his cards

      if (user.role === "player") {
        let playerIndex = user.player

        if (room?.players) {
          // find the active players
          let activeplayers = room.players.filter((player) => !player.inactive)

          // change the flag to true

          if (
            typeof playerIndex !== "undefined" &&
            room?.dealers[playerIndex]
          ) {
            room.dealers[playerIndex].inactive = true;
          }

          if (
            typeof playerIndex !== "undefined" &&
            room?.players[playerIndex]
          ) {
            room.players[playerIndex].inactive = true

            // reveal the player hand , if the active players length is greater than three
            if (activeplayers.length > 3) {
              room.players[playerIndex].exposed = true

              room.occupants.forEach((_, index) => {
                events = events.concat(
                  this.renderState(request.room, room, index, ["hands"])
                )
              })
            }
          }
        }
      }

      // save the room
      this.setRoom(request.room, room)

      return events
    }
  }

  async handleRoomDisconnect(request) {
    console.debug(`${request.user} has disconnected from ${request.room}.`)

    // TODO: process differently if needed
    let events = []

    // fetch the room
    let room = await this.getRoom(request.room)

    // if the room doesn't exist, there's nothing to do!
    if (!room) {
      console.debug(`${request.room} does not exist; ignoring.`)
      return events
    }

    // find the user
    let userIndex = room.occupants.findIndex(
      (o) => o.jid && o.jid == jid(request.user).bare().toString()
    );

    // if the user is not in the room, there's nothing to do!
    if (userIndex < 0) {
      console.debug(`${request.user} is not in ${request.room}; ignoring.`)
      return events
    }

    // get the user
    let user = room.occupants.find(o => o.jid == jid(request.user).bare().toString())

    console.log("user jid obbject ", user)

    // find out how many users are left
    if (room.occupants.length == 0) {
      // no users? destroy the table!

      this.delRoom(request.room)
      return events
    } else {
      // tell everyone
      room.occupants.forEach((o) => 
        events.push({
          event: "roomDisconnect",
          to: o.jid,
          user: user.jid,
          room: request.room,
          nickname: user.nickname,
          type: "unavailable"
        })
      )
    }

    // save the room
    this.setRoom(request.room, room)

    return events
  }

  async handleGameStart(request) {
    console.debug(`${request.user} is ready to play at ${request.room}`)

    let events = []

    // fetch the room
    let room = await this.getRoom(request.room)

    // if the room doesn't exist, there's nothing to do!
    if (!room) {
      console.debug(`${request.room} does not exist; ignoring.`);
      return events
    }

    let dool = new Dool(room)

    // find the user
    let userIndex = dool.data.occupants.findIndex(
      (o) => o.jid && o.jid == jid(request.user).bare().toString()
    )

    // if the user is not in the room, reject them
    if (userIndex < 0) {
      console.debug(`${request.user} is not in ${request.room}; rejecting.`)

      events.push({
        event: "startError",
        user: request.user,
        room: request.room,
      });
      return events
    }

    // If the game has already started, reject it
    // Added the replay code for continuos play, if the player's turn is not score only
    // it should emit the Reject event.

    if (dool.data.started && !dool.getTurn() === "score") {
      console.debug(`A game is already ongoing at ${request.room}; rejecting.`);

      events.push({
        event: "startError",
        user: request.user,
        room: request.room,
      });
      return events
    }

    // If the user has already said they're ready,
    // pretend to take action but don't actually do
    // anything
    if (!!dool.data.occupants[userIndex].ready) {
      console.debug(
        `${request.user} has already said they're ready; ignoring.`
      );

      events.push({
        event: "startError",
        user: request.user,
        room: request.room,
      });
      return events
    }

    // Role has changed, update the user's role
    console.debug(`rover ${request.user} wants to play the game, so change their role to 'player' `);

    // Default role of the user who joins a table is `rover` until he makes a change!
    // Setting user role to `player`, cuz he clicked `play` in frontend :)
    dool.data.occupants[userIndex].role = "player";

    // If the user hasn't said it before, then it's
    // a sign we should actually get up and work
    dool.data.occupants[userIndex].ready = true

    // Tell everyone about it
    dool.data.occupants.forEach((o) => {
      // this is for updating the role has changed
      // from `rover` to `player`
      events.push({
        event: 'roomJoined',
        to: o.jid,
        user: o.jid,
        room: request.room,
        role: o.role,
        nickname: o.nickname,
      });

      events.push({
        event: "start",
        to: o.jid,
        nickname: dool.data.occupants[userIndex].nickname,
        room: request.room,
      })
    })

    // But wait, there's more! If everyone is ready
    // we need to start the game!
    let actualPlayers = dool.data.occupants.filter(
      (occupant) => occupant.role === "player"
    );

    const dealerChange = () => {

      // Checking if the user is active or not
      let activeDealers = room.dealers
        .map((p) => ({ ...p }))
        .filter((p) => !p.inactive)

      const dealerIndex = activeDealers.findIndex((dealer) => dealer.dealer);

      if (dealerIndex !== -1) {
        activeDealers.forEach((dealer, index) => {
          if (index === dealerIndex) {
            dealer.dealer = false
          } else if (index === (dealerIndex + 1) % activeDealers.length) {
            dealer.dealer = true
          } else {
            dealer.dealer = false
          }
        });
      }
      return activeDealers
      // TODO: Check is the user still on the game. i.e., is he an occupant
    };

    if (
      actualPlayers.length > 1 &&
      dool.data.occupants.filter(o => o.role === 'player' && o.ready).length >= 2
    ) {
      // Creating the new dealer array,. in dool data - if
      // I'm creating before the players
      if (!room.dealers) {
        dool.data.dealers = []

        actualPlayers.forEach((o, i) => {
          let dealer = {
            nickname: o.nickname,
            inactive: false,
            dealer: i === 0 ? true : false,
          };

          dool.data.dealers[i] = dealer
        })
      } else {
        dool.data.dealers = dealerChange()
      }

    }

    // Checking if the user is active or not
    let activeDealers = room.dealers?.map((p) => ({ ...p }))?.filter((p) => !p.inactive)

    // --------------Dealers--------------- //

    if (
      actualPlayers.length > 1 &&
      dool.data.occupants.filter(o => o.role === 'player' && o.ready).length == 2
    ) {
      dool.data.started = true
      // Increment the deal number
      dool.incrementDealNumber();

      // add the dealer for making first bid!
      dool.data.bids = []

      // shall we add here the dummy players array with actualPlayers?
      // TODO: need to modify:
      // custom logic for dool
      actualPlayers.push({ ...actualPlayers[0] })
      actualPlayers.push({ ...actualPlayers[1] })

      // Creating occupants.players 
      dool.data.occupants.forEach((occupant) => {
        occupant.players = [];

        // Find players with the same nickname in the playersArray using forEach
        actualPlayers.forEach((player, index) => {
          if (player.nickname === occupant.nickname) {
            occupant.players.push(index);
          }
        });
      })

      actualPlayers = [...new Set([...actualPlayers, ...dool.data.occupants.filter(o => o.role === "player")])];

      // Count the players
      dool.data.players = []

      // Create a teams - Dool specific
      dool.data.teams = dool.data.occupants
        .filter(occupant => occupant.players)
        .map(occupant => occupant.players)

      actualPlayers.forEach((o, i) => {
        let player = {
          nickname: o.nickname,
          team: i % 2
        }

        o.player = i;
        dool.data.players[i] = player
      })

      // Set up the deck
      dool.data.deck = new cards.Deck()

      // Deal out the cards
      dool.dealCards().hands.forEach((hand, i) => {
        dool.data.players[i].hand = hand
      })

      // setting handLength for future use:
      dool.data.handLength = dool.data.players[0].hand.length

      // create dummyPlayers array 
      dool.data.dummyPlayers = JSON.parse(JSON.stringify(dool.data.players))

      // Prepare the tricks space
      dool.data.tricks = new cards.Tricks()

      // Change the roles of non-ready players to 'watcher'
      dool.data.occupants.forEach((o) => {
        if (!o.ready && o.role === 'player') {
          console.log("this people will be going to be watchers")
          o.role = 'watcher';
        }
      });

      // Notify everyone about the role changes
      dool.data.occupants.forEach((o) => {
        events.push({
          event: 'roomJoined',
          to: o.jid,
          user: o.jid,
          room: request.room,
          role: o.role,
          nickname: o.nickname,
        });
      });

      // creating dealer variable:
      dool.data.dealer = activeDealers.findIndex(
        (dealerObj) => dealerObj.dealer
      );

      // Broadcast it all
      dool.data.occupants.forEach((_, occupantIndex) => {
        events = events.concat(
          this.renderState(request.room, dool.data, occupantIndex, ["game"])
        )
      })
    }

    console.log("dool data for ", dool.data)
    // Save the changes and return
    this.setRoom(request.room, dool.data)
    return events
  }

  // TODO: Need to change the level number
  // i.e., level + 6!
  async handleBid(bid) {
    console.debug(`${bid.user} wants to make ${bid.type} bid: ${bid.level}${bid.suit}`)

    let events = []

    // fetch the room
    let room = await this.getRoom(bid.room)

    // if the room doesn't exist, there's nothing to do!
    if (!room) {
      console.debug(`${bid.room} does not exist; ignoring.`)
      return events
    }

    // find the user
    let userIndex = room.occupants.findIndex(
      (o) => o.jid && o.jid == jid(bid.user).bare().toString()
    );

    // If the user is not in the room, reject them
    if (userIndex < 0) {
      console.debug(`${bid.user} is not in ${bid.room}; rejecting.`)

      events.push({
        event: "bidError",
        user: bid.user,
        room: bid.room,
        player: bid.player,
        type: bid.type,
        level: bid.level,
        suit: bid.suit,
        errorText: "You do not belong to this room",
      });
      return events
    }

    // If the user is not a player, reject them
    let player = room.occupants[userIndex].player
    if (typeof player != "number" || player < 0) {
      console.debug(`${bid.user} is not a player in ${bid.room}; rejecting`)

      events.push({
        event: "bidError",
        user: bid.user,
        room: bid.room,
        player: bid.player,
        type: bid.type,
        level: bid.level,
        suit: bid.suit,
        errorText: "You are not a player in this room",
      });
      return events
    }

    // check format
    if ([
      'level',
      'redouble',
      'double',
      'pass'
    ].indexOf(bid.type) == -1
    ) {
      console.error(`Invalid bid: ${bid.type}`)
      events.push({
        event: "bidError",
        user: bid.user,
        room: bid.room,
        player: bid.player,
        type: bid.type,
        level: bid.level,
        suit: bid.suit,
        errorType: "invalid-turn",
        errorText: "Invalid bid",
      });
      return events
    }

    // check format again for level bids
    if (bid.type == 'level') {

      // level must be between 1 and 7
      if (bid.level > 7 || bid.level < 1) {
        console.error(`Invalid bid level: ${bid.level}`)
        events.push({
          event: "bidError",
          user: bid.user,
          room: bid.room,
          player: bid.player,
          type: bid.type,
          level: bid.level,
          suit: bid.suit,
          errorType: "invalid-turn",
          errorText: "Invalid bid level, it should rest between 1 to 7",
        });
        return events
      }

      // suit must be a valid suit or NT
      if (![...SUITS.keys(), 'NT'].includes(bid.suit)) {
        console.error(`Invalid suit: ${bid.suit}`)
        events.push({
          event: "bidError",
          user: bid.user,
          room: bid.room,
          player: bid.player,
          type: bid.type,
          level: bid.level,
          suit: bid.suit,
          errorType: "invalid-turn",
          errorText: "Invalid suit",
        });
        return events
      }
    } else {
      // otherwise, clear extraneous fields
      bid.level = undefined
      bid.suit = undefined
    }

    // If the game has not yet started, reject it
    if (!room.started) {
      console.debug(`The game at ${bid.room} has not yet started!`)

      events.push({
        event: "bidError",
        user: bid.user,
        room: bid.room,
        player: bid.player,
        type: bid.type,
        level: bid.level,
        suit: bid.suit,
        errorType: "invalid-turn",
        errorText: "The game has not yet started",
      });
      return events
    }

    // Load the Dool logic!
    let dool = new Dool(room)

    // Find out whose turn it is
    let turn = dool.getTurn()

    // We can include a "|| !turn.players.includes(player)"
    // here if we don't want to let people keep changing their
    // bids.
    if (turn.type != "bid" || !turn.players.includes(player)) {
      console.debug(`Invalid turn from ${bid.user} at ${bid.room}; rejecting`)

      events.push({
        event: "bidError",
        user: bid.user,
        room: bid.room,
        player: bid.player,
        type: bid.type,
        level: bid.level,
        suit: bid.suit,
        errorType: "invalid-turn",
        errorText: "It is not your turn to bid",
      });
      return events
    }

    // Check if it's a valid bid
    if (!dool.isValidBid(bid.type, bid.level, bid.suit)) {
      console.debug(`Bid of ${bid.type} of ${bid.level} ${bid.suit} by ${bid.user} is invalid; rejecting`)

      events.push({
        event: "bidError",
        user: bid.user,
        room: bid.room,
        player: bid.player,
        type: bid.type,
        level: bid.level,
        suit: bid.suit,
        errorType: "invalid-turn",
        errorText: "It is not a valid bid",
      });
      return events
    }

    // Save the bid
    dool.data.bids.push({ player: player, bidType: bid.type, level: bid.level, suit: bid.suit });

    // for setting the contract here
    // if the bidding end
    if (dool.data.bids.length >= 2 && dool.hasPassed()) {
      let contract = dool.getContract()

      // set the contract in data :) (will decide winner or ...)
      dool.data.contract = contract

      // I should not have done this, I should have created
      // contract in data and then get it the declarer
      // set the declarer in dool data
      dool.data.declarer = contract?.declarer

      // setting the trump suit for gamePlay
      dool.data.tricks.trumpSuit = contract?.suit == 'NT' ? undefined : contract.suit
    }

    // Tell everyone about it
    let nick = dool.data.players[player].nickname
    dool.data.occupants.forEach((o) => {
      events.push({
        event: "bid",
        to: o.jid,
        player: player,
        room: bid.room,
        player: player,
        nickname: nick,
        type: bid.type,
        level: bid.level,
        suit: bid.suit
      });
    });

    turn = dool.getTurn()
    if (turn.type == "score") {

      console.debug("game stage is in `score` now! in handleBid")

      // Setting their `hands` open for showing all hands
      dool.data.players.forEach((_, index) => {
        dool.data.players[index].open = true;
      });

      dool.data.occupants.forEach((o) => {
        o.ready = false
        o.role = "rover"
        if (o.player != undefined) {
          delete o.player
          delete o.players
        }
      })

      dool.data.started = false

      dool.data.score = [0, 0];

      dool.data.occupants.forEach((_, occupantIndex) => {
        events.push(
          ...this.renderState(bid.room, dool.data, occupantIndex, ["score", "scoreHands"])
        );
      });

      delete dool.data.players
      delete dool.data.tricks
      delete dool.data.declarer
      delete dool.data.leader
      delete dool.data.bids
      delete dool.data.score
      delete dool.data.fold
      delete dool.data.contract
      delete dool.data.runningScore
      delete dool.data.dummyPlayers
    }

    // Save the changes and return
    this.setRoom(bid.room, dool.data)
    return events
  }

  async handleCard(card) {
    console.debug(
      `${card.user} has played ${card.suit} of ${card.rank} in ${card.room}`
    );

    let events = [];

    // fetch the room
    let room = await this.getRoom(card.room);

    // if the room doesn't exist, there's nothing to do!
    if (!room) {
      console.debug(`${card.room} does not exist; ignoring.`)
      return events
    }

    // we have to get the userIndex from `room.players`
    // find the user
    let userIndex = room.occupants.findIndex(
      (o) => o.jid && o.jid == jid(card.user).bare().toString()
    );

    // If the user is not in the room, reject them
    if (userIndex < 0) {
      console.debug(`${card.user} is not in ${card.room}; rejecting.`)

      events.push({
        event: "cardError",
        user: card.user,
        room: card.room,
        player: card.player,
        suit: card.suit,
        rank: card.rank,
        errorText: "You do not belong to this room",
      })
      return events
    }

    // If the user is not a player, reject them
    let player = room.occupants[userIndex].player;
    if (typeof player != "number" || player < 0) {
      console.debug(`${card.user} is not a player in ${card.room}; rejecting`)

      events.push({
        event: "cardError",
        user: card.user,
        room: card.room,
        player: card.player,
        suit: card.suit,
        rank: card.rank,
        errorText: "You are not a player in this room",
      });
      return events
    }

    // If the game has not yet started, reject it
    if (!room.started) {
      console.debug(`The game at ${card.room} has not yet started!`)

      events.push({
        event: "cardError",
        user: card.user,
        room: card.room,
        player: card.player,
        suit: card.suit,
        rank: card.rank,
        errorType: "invalid-turn",
        errorText: "The game has not yet started",
      })
      return events
    }

    // Load the Dool logic!
    let dool = new Dool(room)

    // Add the next trick if necessary
    if (dool.isTrickComplete()) {
      dool.data.tricks.addTrick()
    }

    // Assuming we define a function to get the partner index
    const getPartnerIndex = (playerIndex) => {
      // This assumes player 0 is partnered with 2, and 1 with 3.
      return (player + 2) % 4;
    };

    // Find out whose turn it is
    let turn = dool.getTurn()

    // if claim is in progress, do not allow to play
    if (turn.type == "claim") {
      console.debug(
        `Invalid turn from ${card.user} at ${card.room}; rejecting`
      );
      
      events.push({
        event: "cardError",
        user: card.user,
        room: card.room,
        player: player,
        suit: card.suit,
        rank: card.rank,
        errorType: "invalid-turn",
        errorText: "Claim is in progress, you cannot play a card now!",
      });
      return events
    }

    // Check if it's the player's turn or their partner's turn
    let partnerIndex = getPartnerIndex(player);

    /**
     * If its the player's turn or their partner's turn, allow them to play
     */
    if (!["card"].includes(turn.type) && !(turn.players.includes(player) || turn.players.includes(partnerIndex))) {
      console.debug(
        `Invalid turn from ${card.user} at ${card.room}; rejecting`
      );

      events.push({
        event: "cardError",
        user: card.user,
        room: card.room,
        player: player,
        suit: card.suit,
        rank: card.rank,
        errorType: "invalid-turn",
        errorText: "It's not your turn to play!",
      });
      return events
    }

    if (turn.type == "card" && (turn.players.includes(player) || turn.players.includes(partnerIndex))) {

      // Player is either playing their own hand or on behalf of their partner
      let actingPlayer = turn.players.includes(player) ? player : partnerIndex;

      // Now, fetch the hand from which the card will be played
      let handToPlayFrom = dool.data.players[actingPlayer].hand;

      // Figure out what the valid cards are from the acting player's hand
      let validCards = dool.data.tricks.getValidCards(handToPlayFrom);

      // Check if the card played is in the valid cards
      if (!validCards.find((c) => c.rank === card.rank && c.suit === card.suit)) {
        console.debug(`Card ${card.rank} of ${card.suit} cannot be played by ${card.user}; rejecting`);

        events.push({
          event: "cardError",
          user: card.user,
          room: card.room,
          player: actingPlayer,
          suit: card.suit,
          rank: card.rank,
          errorType: "invalid-card",
          errorText: "You cannot play that card now",
        });
        return events;
      }

      // If the card is valid, play the card from the acting player's hand
      dool.play(actingPlayer, {
        suit: card.suit,
        rank: card.rank,
      });

      // Tell everyone about the card played, including who it was played for if it's a partner's turn
      let nick = dool.data.players[actingPlayer].nickname;
      dool.data.occupants.forEach((o) => {
        events.push({
          event: "card",
          to: o.jid,
          player: actingPlayer,
          playedFor: actingPlayer !== player ? player : undefined, // Indicate if the card was played for a partner
          room: card.room,
          nickname: nick,
          suit: card.suit,
          rank: card.rank,
        });
      });

    }

    // set the running score in data (for catchup)
    if (dool.isTrickComplete()) dool.data.runningScore = dool.trickScore()

    // if the last card has been played and then
    // the player chooses to take the trick means
    // we have to send the score
    turn = dool.getTurn()
    if (turn.type == "score") {

      console.debug("game stage is in `score` now!")

      // Setting their `hands` open for showing all hands
      dool.data.players.forEach((_, index) => {
        dool.data.players[index].open = true;
      });

      dool.data.occupants.forEach((o) => {
        o.ready = false
        o.role = "rover"
        if (o.player != undefined) {
          delete o.player
          delete o.players
        }
      })

      dool.data.occupants.forEach((_, occupantIndex) => {
        events.push(
          ...this.renderState(card.room, dool.data, occupantIndex, ["score", "scoreHands"])
        );
      });

      dool.data.started = false

      // for new user
      this.sendScore(dool.data, card.room)

      events = events.concat(this.sendScore(dool.data, card.room))
      delete dool.data.players
      delete dool.data.tricks
      delete dool.data.declarer
      delete dool.data.leader
      delete dool.data.fold
      delete dool.data.score
      delete dool.data.bids
      delete dool.data.contract
      delete dool.data.runningScore
      delete dool.data.dummyPlayers
    }


    // The game will never be completed by a cardplay
    // since we'll have to wait for a take or flip first.
    // So we only need to handle endgame while processing
    // takes and flips!

    // Save the changes and return
    this.setRoom(card.room, dool.data);
    return events
  }

  async handleTake(take) {
    console.debug(`${take.user} is attempting to take the trick in ${take.room}`)

    let events = []

    // fetch the room
    let room = await this.getRoom(take.room)

    // if the room doesn't exist, there's nothing to do!
    if (!room) {
      console.debug(`${take.room} does not exist; ignoring.`)
      return events;
    }

    // find the user
    let userIndex = room.occupants.findIndex(
      (o) => o.jid && o.jid == jid(take.user).bare().toString()
    );

    // If the user is not in the room, reject them
    if (userIndex < 0) {
      console.debug(`${take.user} is not in ${take.room}; rejecting.`)

      events.push({
        event: "takeError",
        user: take.user,
        room: take.room,
        player: take.player,
        errorText: "You do not belong to this room",
      });
      return events
    }

    // If the user is not a player, reject them
    let player = room.occupants[userIndex].player;
    if (typeof player != "number" || player < 0) {
      console.debug(`${take.user} is not a player in ${take.room}; rejecting`)

      events.push({
        event: "takeError",
        user: take.user,
        room: take.room,
        player: take.player,
        errorText: "You are not a player in this room",
      })
      return events
    }

    // If the game has not yet started, reject it
    if (!room.started) {
      console.debug(`The game at ${take.room} has not yet started!`)

      events.push({
        event: "takeError",
        user: take.user,
        room: take.room,
        player: take.player,
        errorType: "invalid-turn",
        errorText: "The game has not yet started",
      })
      return events
    }

    // Load the Dool logic!
    let dool = new Dool(room);

    // Find out whose turn it is
    let turn = dool.getTurn()

    // Only take if it's our turn *and* it's time to
    // take (or flip) the trick
    if (turn.type != "take" || !turn.players.includes(player)) {
      console.debug(
        `Invalid turn from ${take.user} at ${take.room}; rejecting`
      );

      events.push({
        event: "takeError",
        user: take.user,
        room: take.room,
        player: take.player,
        errorType: "invalid-turn",
        errorText: "It is not your turn to take the trick (or flip)",
      });
      return events
    }

    // Actually take the trick!
    dool.take(player)

    // Tell everyone about it
    let nick = dool.data.players[userIndex].nickname
    dool.data.occupants.forEach((o) => {
      events.push({
        event: "take",
        to: o.jid,
        player: player,
        room: take.room,
        player: player,
        nickname: nick,
      })
    })

    // if the last card has been played and then
    // the player chooses to take the trick means
    // we have to send the score
    turn = dool.getTurn()
    if (turn.type == "score") {

      dool.data.occupants.forEach((o) => {
        o.ready = false
        o.role = "rover"
        if (o.player != undefined) {
          delete o.player
          delete o.players
        }
      })

      dool.data.started = false

      // for new user
      this.sendScore(dool.data, take.room)

      events = events.concat(this.sendScore(dool.data, take.room))
      delete dool.data.players
      delete dool.data.tricks
    }

    // Save the changes and return

    this.setRoom(take.room, dool.data)
    return events
  }

  async handleFlip(flip) {
    console.debug(`${flip.user} has flipped in ${flip.room}`)

    let events = []

    // fetch the room
    let room = await this.getRoom(flip.room)

    // if the room doesn't exist, there's nothing to do!
    if (!room) {
      console.debug(`${flip.room} does not exist; ignoring.`)
      return events;
    }

    // find the user
    let userIndex = room.occupants.findIndex(
      (o) => o.jid && o.jid == jid(flip.user).bare().toString()
    );

    // If the user is not in the room, reject them
    if (userIndex < 0) {
      console.debug(`${flip.user} is not in ${flip.room}; rejecting.`)

      events.push({
        event: "flipError",
        user: flip.user,
        room: flip.room,
        player: flip.player,
        errorText: "You do not belong to this room",
      });
      return events
    }

    // If the user is not a player, reject them
    let player = room.occupants[userIndex].player;
    if (typeof player != "number" || player < 0) {
      console.debug(`${flip.user} is not a player in ${flip.room}; rejecting`)

      events.push({
        event: "flipError",
        user: flip.user,
        room: flip.room,
        player: flip.player,
        errorText: "You are not a player in this room",
      })
      return events
    }

    // If the game has not yet started, reject it
    if (!room.started) {
      console.debug(`The game at ${flip.room} has not yet started!`)

      events.push({
        event: "flipError",
        user: flip.user,
        room: flip.room,
        player: flip.player,
        errorType: "invalid-turn",
        errorText: "The game has not yet started",
      })
      return events
    }

    // Load the Dool logic!
    let dool = new Dool(room);

    // Find out whose turn it is
    let turn = dool.getTurn()

    // Only play if it's our turn *and* it's time to
    // flip (or take) the trick
    if (turn.type != "take" || !turn.players.includes(player)) {
      console.debug(
        `Invalid turn from ${flip.user} at ${flip.room}; rejecting`
      );

      events.push({
        event: "flipError",
        user: flip.user,
        room: flip.room,
        player: flip.player,
        errorType: "invalid-turn",
        errorText: "It is not your turn to play a card (or flip)",
      });
      return events
    }

    // Make sure we're allowed to flip
    if (!dool.isFlipAllowed()) {
      console.debug(`${flip.user} is not allowed to flip right now; rejecting`)

      events.push({
        event: "flipError",
        user: flip.user,
        room: flip.room,
        player: flip.player,
        suit: flip.suit,
        rank: flip.rank,
        errorType: "invalid-turn",
        errorText: "You cannot flip right now",
      })
      return events
    }

    // Actually flip!
    dool.data.tricks.flip()

    // The other player takes the trick, since we can't
    // flip multiple times in Dool
    dool.take(dool.getTurn().players.slice(-1).pop())

    // Tell everyone about it
    let nick = dool.data.players[userIndex].nickname
    dool.data.occupants.forEach((o) => {
      events.push({
        event: "flip",
        to: o.jid,
        player: player,
        room: flip.room,
        player: player,
        nickname: nick,
      })
    })

    // if the last card has been played and then
    // the player chooses to take the trick means
    // we have to send the score
    turn = dool.getTurn()
    if (turn.type == "score") {
      dool.data.occupants.forEach((o) => {
        o.ready = false;
        o.role = "rover"
        if (o.player != undefined) {
          delete o.player
          delete o.players
        }
      })
      dool.data.started = false
      // for new user

      this.sendScore(dool.data, flip.room)

      events = events.concat(this.sendScore(dool.data, flip.room))
      delete dool.data.players
      delete dool.data.tricks
    }

    // Save the changes and return

    this.setRoom(flip.room, dool.data)
    return events
  }

  async handleLeader(lead) {
    console.debug(`${lead.user} is attempting to set the side ${lead.side} to open the card in ${lead.room}`)

    let events = []

    // fetch the room
    let room = await this.getRoom(lead.room)

    // if the room doesn't exist, there's nothing to do!
    if (!room) {
      console.debug(`${lead.room} does not exist; ignoring.`)
      return events;
    }

    // find the user
    let userIndex = room.occupants.findIndex(
      (o) => o.jid && o.jid == jid(lead.user).bare().toString()
    );

    // If the user is not in the room, reject them
    if (userIndex < 0) {
      console.debug(`${lead.user} is not in ${lead.room}; rejecting.`)

      events.push({
        event: "leadingError",
        user: lead.user,
        room: lead.room,
        player: lead.player,
        errorText: "You do not belong to this room",
      });
      return events
    }

    // If the user is not a player, reject them
    let player = room.occupants[userIndex].player;
    if (typeof player != "number" || player < 0) {
      console.debug(`${lead.user} is not a player in ${lead.room}; rejecting`)

      events.push({
        event: "leadingError",
        user: lead.user,
        room: lead.room,
        player: lead.player,
        errorText: "You are not a player in this room",
      })
      return events
    }

    // If the game has not yet started, reject it
    if (!room.started) {
      console.debug(`The game at ${lead.room} has not yet started!`)

      events.push({
        event: "leadingError",
        user: lead.user,
        room: lead.room,
        player: lead.player,
        errorType: "invalid-turn",
        errorText: "The game has not yet started",
      })
      return events
    }

    // Load the Dool logic!
    let dool = new Dool(room);

    // Find out whose turn it is
    let turn = dool.getTurn()

    // Only lead if it's our turn *and* it's time to
    // lead (or flip) the trick
    if (turn.type != "leading" || !turn.players.includes(player)) {
      console.debug(
        `Invalid turn from ${lead.user} at ${lead.room}; rejecting`
      );

      events.push({
        event: "leadingError",
        user: lead.user,
        room: lead.room,
        player: lead.player,
        errorType: "invalid-turn",
        errorText: "It is not your turn to select the opening lead",
      });
      return events
    }

    let declarerTeam = dool.data.teams.find(team => team.includes(dool.data.declarer));

    // Check if the side is not part of the declarer's team
    let isSideNotInDeclarerTeam = declarerTeam && !declarerTeam.includes(lead.side);

    if(isSideNotInDeclarerTeam) {
      // good we can proceed
      dool.data.leader = +lead.side
    }

    // Tell everyone about it
    let nick = dool.data.players[userIndex].nickname

    dool.data.occupants.forEach((o) => {
      events.push({
        event: "leading",
        to: o.jid,
        player: player,
        room: lead.room,
        nickname: nick,
        side: lead.side
      })
    })

    // Save the changes and return
    this.setRoom(lead.room, dool.data);
    return events
  }

  async handleChat(chat) {
    console.debug(
      `${chat.from} sent a message of ${chat.text} to ${chat.room}`
    );

    let events = []

    // fetch the room
    let room = await this.getRoom(chat.room)

    // if the room doesn't exist, there's nothing to do!
    if (!room) {
      console.debug(`${chat.room} does not exist; ignoring.`)
      return events;
    }

    // find the user
    let userIndex = room.occupants.findIndex(
      (o) => o.jid && o.jid == jid(chat.from).bare().toString()
    );

    // If the user is not in the room, reject them
    if (userIndex < 0) {
      console.debug(`${chat.user} is not in ${chat.room}; rejecting.`)

      events.push({
        event: "chatError",
        user: chat.from,
        room: chat.room,
        errorText: "You do not belong to this room",
      })
      return events
    }

    let nickname = room.occupants[userIndex].nickname

    let message = {
      nickname,
      from: chat.from,
      text: chat.text,
    };

    //store the message
    room.messages.push(message);

    //send message to every one
    room.occupants.forEach((o) => {
      events.push({
        event: "chat",
        to: o.jid,
        room: chat.room,
        text: chat.text,
        nickname: nickname,
      })
    })

    // Save the changes and return
    this.setRoom(chat.room, room)
    return events;
  }

  async handleFold(fold) {
    let events = []

    // fetch the room
    let room = await this.getRoom(fold.room)

    // if the room doesn't exist, there's nothing to do!
    if (!room) {
      console.debug(`${fold.room} does not exist; ignoring.`)
      return events;
    }

    // find the user
    let userIndex = room.occupants.findIndex(
      (o) => o.jid && o.jid == jid(fold.user).bare().toString()
    );

    // If the user is not in the room, reject them
    if (userIndex < 0) {
      console.debug(`${fold.user} is not in ${fold.room}; rejecting.`)

      events.push({
        event: "foldError",
        user: fold.user,
        room: fold.room,
        player: fold.player,
        errorText: "You do not belong to this room",
      });
      return events
    }

    // If the user is not a player, reject them
    let player = room.occupants[userIndex].player;
    if (typeof player != "number" || player < 0) {
      console.debug(`${fold.user} is not a player in ${fold.room}; rejecting`)

      events.push({
        event: "foldError",
        user: fold.user,
        room: fold.room,
        player: fold.player,
        errorText: "You are not a player in this room",
      })
      return events
    }

    // If the game has not yet started, reject it
    if (!room.started) {
      console.debug(`The game at ${fold.room} has not yet started!`)

      events.push({
        event: "foldError",
        user: fold.user,
        room: fold.room,
        player: fold.player,
        errorType: "invalid-turn",
        errorText: "The game has not yet started",
      })
      return events
    }

    // Load the Dool logic!
    let dool = new Dool(room);

    // Find out whose turn it is
    let turn = dool.getTurn()

    let getTeam = dool.data.teams.find(team => team.includes(player))
    console.log("turn fold ", turn, player, getTeam)

    // Only fold if it's our turn
    if (!["swap", "bid", "leading", "card", "claim", "take"].includes(turn.type)) {
      console.debug(
        `Invalid turn from ${fold.user} at ${fold.room}; rejecting`
      );

      events.push({
        event: "foldError",
        user: fold.user,
        room: fold.room,
        player: fold.player,
        errorType: "invalid-turn",
        errorText: "It is not your turn to fold",
      });
      return events
    }

    dool.data.fold = true

    // Setting their `hands` open for showing all hands
    dool.data.players.forEach((_, index) => {
        dool.data.players[index].open = true;
    });

    // Tell everyone about it
    let nick = dool.data.players[userIndex].nickname

    dool.data.occupants.forEach((o) => {
      events.push({
        event: "fold",
        to: o.jid,
        player: player,
        room: fold.room,
        nickname: nick,
      })
    })

    turn = dool.getTurn()
    if (turn.type == "score") {

      console.debug("game stage is in `score` now! in `handleFold`")

      dool.data.occupants.forEach((o) => {
        o.ready = false
        o.role = "rover"
        if (o.player != undefined) {
          delete o.player
          delete o.players
        }
      })

      dool.data.started = false

      dool.data.score = [0, 0];

      dool.data.occupants.forEach((_, occupantIndex) => {
        events.push(
          ...this.renderState(fold.room, dool.data, occupantIndex, ["score", "scoreHands"])
        );
      });

      delete dool.data.players
      delete dool.data.tricks
      delete dool.data.declarer
      delete dool.data.leader
      delete dool.data.bids
      delete dool.data.score
      delete dool.data.fold
      delete dool.data.contract
      delete dool.data.runningScore
      delete dool.data.dummyPlayers
    }

    // Save the changes and return
    this.setRoom(fold.room, dool.data);
    return events
  }

  async handleClaim(claim) {
    console.debug(
      `${claim.user} has claimed in ${claim.room}`
    );
    let events = []

    // fetch the room
    let room = await this.getRoom(claim.room)

    // if the room doesn't exist, there's nothing to do!
    if (!room) {
      console.debug(`${claim.room} does not exist; ignoring.`)
      return events;
    }

    // find the user
    let userIndex = room.occupants.findIndex(
      (o) => o.jid && o.jid == jid(claim.user).bare().toString()
    );

    // If the user is not in the room, reject them
    if (userIndex < 0) {
      console.debug(`${claim.user} is not in ${claim.room}; rejecting.`)

      events.push({
        event: "claimError",
        user: claim.user,
        room: claim.room,
        player: claim.player,
        errorText: "You do not belong to this room",
      });
      return events
    }

    // If the user is not a player, reject them
    let player = room.occupants[userIndex].player;

    if (typeof player != "number" || player < 0) {
      console.debug(`${claim.user} is not a player in ${claim.room}; rejecting`)

      events.push({
        event: "claimError",
        user: claim.user,
        room: claim.room,
        player: claim.player,
        errorText: "You are not a player in this room",
      })
      return events
    }

    // If the game has not yet started, reject it
    if (!room.started) {
      console.debug(`The game at ${claim.room} has not yet started!`)

      events.push({
        event: "claimError",
        user: claim.user,
        room: claim.room,
        player: claim.player,
        errorType: "invalid-turn",
        errorText: "The game has not yet started",
      })
      return events
    }

    // Tell everyone about it
    let nick = room.players[userIndex].nickname

    // Load the Dool logic!
    let dool = new Dool(room);

    // Add the next trick if necessary
    if (dool.isTrickComplete()) {
      dool.data.tricks.addTrick()
    }

    // if no tricks means, cannot claim
    if(dool.data.tricks.length < 1) {
      console.debug(`${claim.room} there are no tricks, can't claim  now;`)

      events.push({
        event: "claimError",
        user: claim.user,
        room: claim.room,
        player: player,
        proposal: claim.proposal,
        nickname: nick,
        errorType: "invalid-turn",
        errorText: "There are no tricks (at least play one card to claim)",
      });
      return events
    }
    
    // get the lastMost trick:
    let currentTrick = dool.data.tricks[dool.data.tricks.length - 1]

    // get the claimObj index, which will have the claim Class
    function findLastClaimIndex(trick) {
      for (let i = trick.length - 1; i >= 0; i--) {
        if (trick[i].proposal) {
          return i;
        }
      }
      return null; // Return -1 if no proposal is found
    }

    // Compare the given objects has the same proposal
    function compareProposals(obj1, obj2) {
      const keys1 = Object.keys(obj1);
      const keys2 = Object.keys(obj2);

      if (keys1.length !== keys2.length) {
        return false;
      }

      for (let key of keys1) {
        if (obj1[key] !== obj2[key]) {
          return false;
        }
      }

      return true;
    }

    // which will have the matching claim object
    let claimIndex = findLastClaimIndex(currentTrick)

    /**
     * There are three cases before implement claim logic:
     * 
     * 1. Existing claim (Check claim instance is there and Is that activeClaim and Compare proposal)
     * 2. If Claim instance present, then check is it active
     * 3. If the above cases not true, then add new Claim()
     */

    let claimRejected = false

    /**
     * Calculate the completed and remaining tricks 
     * for ease of process
     */
    let completedTricks = 0;
    let remainingTricks = 0

    // calculate completed tricks length
    for (let trick of dool.data.tricks) {
      if (trick.filter(trickEl => trickEl.card).length == dool.data.players.length) {
        completedTricks += 1
      }
    }

    // calculate remaining tricks length
    remainingTricks = dool.data.handLength - completedTricks

    // CASE 1: Existing claim
    if (
      // implement claimClass (matchClass) && activeClaim && compare proposal
      findLastClaimIndex(currentTrick) !== null
      && currentTrick[claimIndex].isClaimGoingOn()
      && compareProposals(currentTrick[claimIndex].proposal, claim.proposal)
    ) {
      // push the playerIndex to whatever array based on the mode
      if (claim.mode === "rejected") {
        // TODO: do check if the player index already there before push for both
        currentTrick[claimIndex].rejectedBy.push(player)
        claimRejected = true;
      } else {
        currentTrick[claimIndex].acceptedBy.push(player)
      }
    } else if (
      // matchClass  && activeClaim
      currentTrick.some((claim) => claim.proposal && claim.isClaimGoingOn())
    ) {
      console.debug(`${claim.room} claim request is already in a progress!`)

      events.push({
        event: "claimError",
        user: claim.user,
        room: claim.room,
        nickname: nick,
        player: player,
        proposal: claim.proposal,
        errorText: "Claim request already in progress",
      });
      return events
    } else {
      if (
        claim.mode == "rejected"
      ) {
        console.debug(`${claim.room} does not have claim like this, so we cannot reject :)`)

        events.push({
          event: "claimError",
          user: claim.user,
          room: claim.room,
          proposal: claim.proposal,
          nickname: nick,
          player: player,
          errorText: "Claim does not exist, can't reject;",
        });
        return events
      } else {
        // Add a new Claim()
        console.log("CASE 3: ADD NEW CLAIM!")

        let totalProposal = claim.proposal;

        let totalAmount = Object.values(totalProposal).reduce((acc, currentValue) => acc + currentValue, 0);

        /**
         * You cannot claim the tricks that doesn't exist right?
         * Emit an error if the proposal amount is greater than
         * the remaining tricks value.
         */
        if (totalAmount > remainingTricks) {
          console.error(`${claim.user} tricks are not available to claim`)

          events.push({
            event: "claimError",
            user: claim.user,
            room: claim.room,
            proposal: claim.proposal,
            nickname: nick,
            player: player,
            errorText: `${claim.proposal[player]} tricks are not available to claim`,
          });
          return events
        }

        // create the accepted array:
        let acceptedUsers = [player] // add userIndex first then add the others

        // create the rejected array:
        let rejectedUsers = []

        // get activePlayers index:
        // need to have the players' indices (if the player's exposed is true
        // then don't include their indices in participants)

        // For Dool, only two players' confirmation is enough, 
        // so push only two players
        let participants = []

        dool.data.players.forEach((player, index) => {
          if (!player.inactive) {
            participants.push(index);
          }
        });

        // TODO: Don't do this, do something else
        participants.splice(-2)

        // first add the claimObj in currentTrick
        currentTrick.push(new cards.Trick().addClaim(claim.proposal, acceptedUsers, rejectedUsers, participants))

        // Logic for getting player team, 
        let claimPlayerTeam = dool.data.teams.find(team => team.includes(player))

        // Setting their hands' / in-player's open value to true.
        dool.data.players.forEach((_, index) => {
          if (claimPlayerTeam.includes(index)) {
            dool.data.players[index].open = true;
          }
        });

        // Calling renderState to send the claimPlayerTeam's 
        // hand to the others.
        dool.data.occupants.forEach((_, occupantIndex) => {
          events.push(
            ...this.renderState(claim.room, dool.data, occupantIndex, ["hands"])
          );
        });
      }
    } 

    dool.data.occupants.forEach((o) => {
      events.push({
        event: "claim",
        to: o.jid,
        proposal: claim.proposal,
        player: player,
        room: claim.room,
        nickname: nick,
        mode: claimRejected ? "rejected" : "accepted"
      })
    })

    // Find out whose turn it is
    let turn = dool.getTurn()

    if (turn.type == "score") {
      // calculate score, if claim accepted
      if(!dool.data.runningScore) {
        dool.data.runningScore = [0, 0]
      }

      let claimValue = currentTrick.getClaim()

      // add/update the running score
      if([0].includes(claimValue.acceptedBy[0])) {
        dool.data.runningScore[0] += claimValue.proposal[claimValue.acceptedBy[0]]
        dool.data.runningScore[1] += remainingTricks - claimValue.proposal[claimValue.acceptedBy[0]]
      } else {
        dool.data.runningScore[1] += claimValue.proposal[claimValue.acceptedBy[0]]
        dool.data.runningScore[0] += remainingTricks - claimValue.proposal[claimValue.acceptedBy[0]]
      }

      // finally game end, emit the deal score
      let dealScore = dool.getDoolScore(dool.data.runningScore)
      dool.data.score = dealScore
      if(dool.data.dealScores) {
        if(dool.data.dealNumber === undefined)
          throw new Error("dealNumber is not defined")
        dool.data.dealScores[dool.data.dealNumber] = dealScore
      } else {
        dool.data.dealScores = [];
        if(dool.data.dealNumber === undefined)
          throw new Error("dealNumber is not defined")
        dool.data.dealScores[dool.data.dealNumber] = dealScore
      }

      // Setting their `hands` open for showing all hands
      dool.data.players.forEach((_, index) => {
        dool.data.players[index].open = true;
      });

      dool.data.occupants.forEach((o) => {
        o.ready = false
        o.role = "rover"
        if (o.player != undefined) {
          delete o.player
          delete o.players
        }
      })

      dool.data.started = false

      dool.data.occupants.forEach((_, occupantIndex) => {
        events.push(
          ...this.renderState(claim.room, dool.data, occupantIndex, ["score", "scoreHands"])
        );
      });

      delete dool.data.players
      delete dool.data.tricks
      delete dool.data.declarer
      delete dool.data.leader
      delete dool.data.bids
      delete dool.data.score
      delete dool.data.fold
      delete dool.data.contract
      delete dool.data.runningScore
      delete dool.data.dummyPlayers
    }

    // Save the changes and return
    this.setRoom(claim.room, dool.data);
    return events
  }

  async handleSwap(swap) {
    console.debug(`${swap.user} has swapped in ${swap.room}`);

    let events = []

    // fetch the room
    let room = await this.getRoom(swap.room)

    // if the room doesn't exist, there's nothing to do!
    if (!room) {
      console.debug(`${swap.room} does not exist; ignoring.`)
      return events;
    }

    // find the user
    let userIndex = room.occupants.findIndex(
      (o) => o.jid && o.jid == jid(swap.user).bare().toString()
    );

    // If the user is not in the room, reject them
    if (userIndex < 0) {
      console.debug(`${swap.user} is not in ${swap.room}; rejecting.`)

      events.push({
        event: "swapError",
        user: swap.user,
        room: swap.room,
        player: swap.player,
        errorText: "You do not belong to this room",
      });
      return events
    }

    // If the user is not a player, reject them
    let player = room.occupants[userIndex].player;

    if (typeof player != "number" || player < 0) {
      console.debug(`${swap.user} is not a player in ${swap.room}; rejecting`)

      events.push({
        event: "swapError",
        user: swap.user,
        room: swap.room,
        player: swap.player,
        errorText: "You are not a player in this room",
      })
      return events
    }

    // If the game has not yet started, reject it
    if (!room.started) {
      console.debug(`The game at ${swap.room} has not yet started!`)

      events.push({
        event: "swapError",
        user: swap.user,
        room: swap.room,
        player: swap.player,
        errorType: "invalid-turn",
        errorText: "The game has not yet started",
      })
      return events
    }

    // Tell everyone about it
    let nick = room.players[userIndex].nickname

    // Load the Dool logic!
    let dool = new Dool(room);

    // Get a user team and check the user is a part of that team?
    let myTeam = this.getTeam(dool.data.teams, player)

    // setting player `swap` -> true
    dool.data.players.forEach((player, index) => {
      if (myTeam.includes(index)) {
        dool.data.players[index].swap = true
      }
    })

    if (swap.noSwap) {
      console.debug(`${player} does not want to swap`)

      dool.data.occupants.forEach((o) => {
        events.push({
          event: "swap",
          to: o.jid,
          player: player,
          room: swap.room,
          nickname: nick,
        })
      })
    } else {
      // for ease of process
      const swapHands = swap.swapHands

      // Function to remove cards from a player's hand
      function removeCardsFromHand(player, cardsToRemove) {
        cardsToRemove.forEach(cardToRemove => {
          const index = player.hand.findIndex(card => card.suit === cardToRemove.suit && card.rank === cardToRemove.rank);

          // emit an error, if the card was not found
          if (index == -1) {
            console.debug(`There are no cards found`)

            events.push({
              event: "swapError",
              user: swap.user,
              room: swap.room,
              errorText: "Card not found in hand to swap",
            })
            return events
          } else {
            // Remove card from hand
            player.hand.splice(index, 1);
          }
        });
      };

      // Function to add cards to a player's hand
      function addCardsToHand(player, cardsToAdd) {
        cardsToAdd.forEach(cardToAdd => {
          const isCardPresent = player.hand.some(card => card.suit === cardToAdd.suit && card.rank === cardToAdd.rank);
          if (!isCardPresent) {
            player.hand.push(cardToAdd); // Add card to hand if not already present
          }
        });
      };

      // Loop for removing the cards from the player hands
      swapHands.forEach(swap => {
        dool.data.players.forEach((player, index) => {
          // If the player's team matches 'from' or 'to' in swapHands
          if (myTeam.includes(+swap.from) && player.hand) {
            if (index == swap.from) {
              removeCardsFromHand(player, swap.subHand);
            }
          }
        });
      });

      // Loop for adding the cards to the player hands
      swapHands.forEach(swap => {
        dool.data.players.forEach((player, index) => {
          if (myTeam.includes(+swap.to) && player.hand) {
            if (index == swap.to) {
              addCardsToHand(player, swap.subHand);
            }
          }
        });
      });

      // set the updated hands to `scorePlayers`
      dool.data.dummyPlayers = JSON.parse(JSON.stringify(dool.data.players))

      dool.data.occupants.forEach((o) => {
        events.push({
          event: "swap",
          to: o.jid,
          player: player,
          room: swap.room,
          nickname: nick,
        })
      })

      // For sending hands to the own player
      events.push(...this.renderState(swap.room, dool.data, userIndex, "subHand"))

      // For sending hands to Kibitzers:
      dool.data.occupants.forEach((o, occupantIndex) => {
        if (["watcher", "rover"].includes(o.role)) 
          events.push(
            ...this.renderState(swap.room, dool.data, occupantIndex, "subHand")
          );
      });
    }

    // Save the changes and return
    this.setRoom(swap.room, dool.data);
    return events
  }

  async handleUndo(undo) {
    console.debug(`${undo.user} has undo-ed in ${undo.room}`);

    let events = []

    // fetch the room
    let room = await this.getRoom(undo.room)

    // if the room doesn't exist, there's nothing to do!
    if (!room) {
      console.debug(`${undo.room} does not exist; ignoring.`)
      return events;
    }

    // find the user
    let userIndex = room.occupants.findIndex(
      (o) => o.jid && o.jid == jid(undo.user).bare().toString()
    );

    // If the user is not in the room, reject them
    if (userIndex < 0) {
      console.debug(`${undo.user} is not in ${undo.room}; rejecting.`)

      events.push({
        event: "undoError",
        user: undo.user,
        room: undo.room,
        player: undo.player,
        errorText: "You do not belong to this room",
      });
      return events
    }

    // If the user is not a player, reject them
    let player = room.occupants[userIndex].player;

    if (typeof player != "number" || player < 0) {
      console.debug(`${undo.user} is not a player in ${undo.room}; rejecting`)

      events.push({
        event: "undoError",
        user: undo.user,
        room: undo.room,
        player: undo.player,
        errorText: "You are not a player in this room",
      })
      return events
    }

    // If the game has not yet started, reject it
    if (!room.started) {
      console.debug(`The game at ${undo.room} has not yet started!`)

      events.push({
        event: "undoError",
        user: undo.user,
        room: undo.room,
        player: undo.player,
        errorType: "invalid-turn",
        errorText: "The game has not yet started",
      })
      return events
    }

    // Tell everyone about it
    let nick = room.players[userIndex].nickname

    // Load the Dool logic!
    let dool = new Dool(room);

    // Get a user team and check the user is a part of that team?
    let myTeam = this.getTeam(dool.data.teams, player)

    // get the lastMost trick:
    let currentTrick = dool.data.tricks[dool.data.tricks.length - 1]

    /**
     * For making undo request:
     */
    // TODO: check the trick is there?
    // if no tricks means, cannot undo
    if (dool.data.tricks.length < 1) {
      console.debug(`${undo.room} there are no tricks, can't undo now;`)

      events.push({
        event: "undoError",
        user: undo.user,
        room: undo.room,
        player: player,
        nickname: nick,
        errorType: "invalid-turn",
        errorText: "There are no tricks (at least one card needed, to request undo)",
      });
      return events
    }

    // if no cards in the current trick do this
    if (currentTrick.length == 0) {
      console.debug(`${undo.room} there are no cards in the current trick, can't undo now;`)

      events.push({
        event: "undoError",
        user: undo.user,
        room: undo.room,
        player: player,
        nickname: nick,
        errorType: "invalid-turn",
        errorText: "There are no cards in this trick (at least one card needed, to request undo)",
      });
      return events
    }

    // TODO: Reject if the lastCard in a trick is not same as the undo-card?!

    // TODO: Reject if the lastPlayer   "     "     "
    
    // Function for getting last undo Index:
    function findLastUndoIndex(trick) {
      for (let i = trick.length - 1; i >= 0; i--) {
        if (trick[i].mode) {
          return i;
        }
      }
      return null; // Return -1 if no proposal is found
    }

    // which will have the matching claim object
    let undoIndex = findLastUndoIndex(currentTrick)

    const { rank, suit } = undo.card

    if (undo.mode == "request") {
      let rejected = []
      let accepted = []
      // if it is the new undo request, create an instance
      currentTrick.push(new Undo(undo.requester, rejected, accepted, undo.mode))

      dool.data.occupants.forEach((o) => {
        events.push({
          event: "undo",
          to: o.jid,
          player: player,
          room: undo.room,
          nickname: nick,
          card: undo.card,
          mode: "request",
        })
      })

      this.setRoom(undo.room, dool.data);
      return events
    } else if(undo.mode == "accepted") {
      currentTrick[undoIndex].accepted.push(player)
      currentTrick[undoIndex].mode = "accepted"

      if(currentTrick[currentTrick.length - 2].mode == "accepted") {
        console.debug(`${undo.room} cannot undo prev-o-prev cards, can't undo now;`)

        events.push({
          event: "undoError",
          user: undo.user,
          room: undo.room,
          player: player,
          nickname: nick,
          errorType: "invalid-turn",
          errorText: "Cannot undo now",
        });
        return events
      };

      if (currentTrick[undoIndex].accepted.length > 0) {

        // If currentTrick doesn't have any cards and doing undo
        // means, we have to get the second last trick and do the actions!
        if (currentTrick.getCards().length == 0) {
          // Remove the newly added trick
          dool.data.tricks.pop()

          // Call undo for removing the last card!
          dool.data.tricks.undo()

          // put back the card to the particular player
          dool.data.players[currentTrick[undoIndex].player].hand.push({ suit: suit, rank: rank })
        } else {
          // remove the last card!
          dool.data.tricks.undo()
          undoIndex = findLastUndoIndex(currentTrick)

          // put back the card to the particular player
          dool.data.players[currentTrick[undoIndex].player].hand.push({ suit: suit, rank: rank })
        }

        dool.data.occupants.forEach((o) => {
          events.push({
            event: "undo",
            to: o.jid,
            player: player,
            room: undo.room,
            nickname: nick,
            card: undo.card,
            mode: "accepted",
          })
        })

        this.setRoom(undo.room, dool.data);
        return events
      }
    } else if(undo.mode == "rejected") {
      currentTrick[undoIndex].rejected.push(player)
      currentTrick[undoIndex].mode = "rejected"

      if (currentTrick[undoIndex].rejected.length > 0) {

        dool.data.occupants.forEach((o) => {
          events.push({
            event: "undo",
            to: o.jid,
            player: player,
            room: undo.room,
            nickname: nick,
            card: undo.card,
            mode: "rejected",
          })
        })

        this.setRoom(undo.room, dool.data);
        return events
      }
    }

    /**
     * For deciding on the undo request:
     */
    // TODO: what if the claim is raised in mean time?, you could have made a claim based on that right?
    // TODO: if mode - `rejected`, then send a txt message to the requester
    // TODO: else - `accepted`, 
    // 1. remove the card from the current trick
    // 2. then get the requested hand based on the team (future), 
    // 3. put the card back in the hand
    // 4. send stanza to the requester with the card

    // Save the changes and return
    this.setRoom(undo.room, dool.data);
    return events
  }

  sendScore(room, roomID) {
    let dool = new Dool(room);
    let score = dool.getDoolScore();
    dool.data.score = score
    if(dool.data.dealScores) {
      if(dool.data.dealNumber === undefined)
        throw new Error("dealNumber is not defined")
      dool.data.dealScores[dool.data.dealNumber] = score
    } else {
      dool.data.dealScores = [];
      if(dool.data.dealNumber === undefined)
        throw new Error("dealNumber is not defined")
      dool.data.dealScores[dool.data.dealNumber] = score
    }

    let events = [];
    dool.data.occupants.forEach((_, occupantIndex) => {
      events.push(
        ...this.renderState(roomID, dool.data, occupantIndex, ["score"])
      );
    });

    return events;
  }

  getTeam(teams, player) {
    return teams.find(team => team.includes(player))
  }

  /**
   * Listeners for incoming events
   */
  addHandler(event, handler) {
    if (!this.transmitter) {
      throw "No transmitter specified! Please run the listen() command first"
    }

    // Make sure we have "this" accessible
    handler = handler.bind(this)

    this.transmitter.on(event, async (e) =>
      this.processEvents(await handler(e))
    )
  }

  listen(transmitter) {
    this.transmitter = transmitter

    this.addHandler("roomJoin", this.handleRoomJoin)
    this.addHandler("roomDisconnect", this.handleRoomDisconnect)
    this.addHandler("roomExit", this.handleRoomExit)
    this.addHandler("start", this.handleGameStart)
    this.addHandler("bid", this.handleBid)
    this.addHandler("card", this.handleCard)
    this.addHandler("flip", this.handleFlip)
    this.addHandler("chat", this.handleChat)
    this.addHandler("take", this.handleTake)
    this.addHandler("leading", this.handleLeader)
    this.addHandler("fold", this.handleFold)
    this.addHandler("claim", this.handleClaim)
    this.addHandler("swap", this.handleSwap)
    this.addHandler("undo", this.handleUndo)
  }
}
